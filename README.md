FrontEndStartedKit
==================

Es un kit para aquellos front-end que quieren ir adquiriendo nuevos métodos de trabajo, utilizando nuevas tecnologías y buenas practicas.

Se utilizan [Stylus](http://learnboost.github.io/stylus/) como pre procesador de CSS y [Jade](http://jade-lang.com/) como template de HTML. También se esta utilizando la librería [RequireJS](http://requirejs.org/) para ordenar las dependencias y modularizar así la app.

Dentro de las dependencias se encuentran un stack de diferentes herramientas de desarrollo como: 
 * jQuery
 * Modernizr
 * Bootstrap
 * Flexslider
 * Lightbox 
 * ScrollTo 
 * Parallax 
 * Masonry 

Todas traídas desde [jsdelivr](http://www.jsdelivr.com/index.php) con sus potentes CDN para hacer un desarrollo mas rápido sin la necesidad de descargar archivos.

Cualquier cambios y sugerencia es bienvenida.
