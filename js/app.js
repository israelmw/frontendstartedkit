// Place third party dependencies in the lib folder
//
// Configure loading modules from the lib directory,
// except 'app' ones, 
requirejs.config({
		"baseUrl": "js/lib",
		"paths": {
			"app": "../app",
			"async" : "async",
			"helper" : "helper",
			"modernizr": "//cdn.jsdelivr.net/modernizr/2.6.3/modernizr.min",
			"jquery": "//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min",
			"flexslider": "//cdn.jsdelivr.net/flexslider/2.1/jquery.flexslider-min",
			"lightbox": "//cdn.jsdelivr.net/lightbox2/2.6/js/lightbox-2.6.min",
			"fancybox": "//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min",
			"scrollto": "//cdn.jsdelivr.net/jquery.scrollto/1.4.6/jquery.scrollTo.min",
			"parallax": "//cdn.jsdelivr.net/jquery.parallax/1.1.3/jquery.parallax-1.1.3",
			"bootstrap": "//cdn.jsdelivr.net/bootstrap/3.1.1/js/bootstrap.min.js",
			"masonry": "//cdn.jsdelivr.net/masonry/3.1.4/masonry.min"
		}
});


// Load the main app module to start the app
requirejs(["app/main"]);
